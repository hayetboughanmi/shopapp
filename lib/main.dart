import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:My_projects/modules/splash/splash_screen.dart';
import 'package:My_projects/shared/bloc_observer.dart';
import 'package:My_projects/shared/components/constants.dart';
import 'package:My_projects/shared/cubit/cubit.dart';
import 'package:My_projects/shared/cubit/states.dart';
import 'package:My_projects/shared/network/local/cache_helper.dart';
import 'package:My_projects/shared/network/remote/dio_helper.dart';
import 'package:My_projects/shared/styles/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();
  DioHelper.init();
  await CacheHelper.init();
  token = CacheHelper.getData(key: 'token');
  print(token);
  bool isDark = CacheHelper.getBoolean(key: 'isDark');
  runApp(ShopApp());
}

class ShopApp extends StatelessWidget {
  final bool isDark;

  ShopApp({this.isDark});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (BuildContext context) => ShopCubit()
              ..getHomeData()
              ..getCategories()
              ..getFavorites()
              ..getUserData()
              ..getCarts()),
      ],
      child: BlocConsumer<ShopCubit, ShopStates>(
        listener: (context, state) {},
        builder: (context, state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: lightMode,
            // darkTheme: darkMode,
            title: 'MyApp',
            home: SplashScreen(),
          );
        },
      ),
    );
  }
}
